var gulp = require ( 'gulp');
var sass = require ('gulp-sass');
var concat = require('gulp-concat');
var wait = require('gulp-wait');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

gulp.task('styles', function () {
    gulp.src('assets/src/scss/**/master.scss')
    .pipe(wait(500))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('assets/dist/css'));

    gulp.src('assets/src/scss/**/master.scss')
    .pipe(wait(500))
    .pipe(rename('master.min.css'))
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('css'));
});

gulp.task('scripts', function() {
  gulp.src('assets/src/js/*.js')
    .pipe(concat('master.js'))
    .pipe(gulp.dest('resources/dist/js'));

    gulp.src('assets/dist/js/master.js')
    .pipe(minify())
    .pipe(gulp.dest('js'));
});

gulp.task('plugins', function() {
  gulp.src('assets/src/plugins/*.js')
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest('assets/dist/js'));

    gulp.src('assets/dist/js/plugins.js')
    .pipe(minify())
    .pipe(gulp.dest('js'));
});

gulp.task('watch', function(){
    gulp.watch('assets/src/scss/**/*.scss',['styles']);
    gulp.watch('assets/src/js/*.js',['scripts']);
    gulp.watch('assets/src/plugins/*.js',['plugins']);
});

gulp.task('default',['styles', 'scripts', 'plugins']);

